#!/usr/bin/env groovy

properties([
  [$class: 'GithubProjectProperty', displayName: 'JAVA/AWS Deploy Pipeline', projectUrlStr: 'https://vps635270.ovh.net/ajc/imago/'],
  buildDiscarder(logRotator(artifactNumToKeepStr: '5', daysToKeepStr: '15'))
])

node {
  def gitRepo = 'https://vps635270.ovh.net/ajc/imago.git'
  stage('Checkout'){
    git branch: 'master', credentialsId: 'imago', url: gitRepo
    stash name : 'docker', includes : 'Dockerfile'
    stash name : 'aws', includes : 'aws/*'
  }

  stage('Build'){
    withMaven(jdk: 'java-sdk8', maven: 'mvn-3.1.1', tempBinDir: '') {
      sh "mvn clean verify"
    }
    step([$class: 'JUnitResultArchiver', testResults: 'target/surefire-reports/*.xml'])
    step([$class: 'JUnitResultArchiver', testResults: 'target/failsafe-reports/*.xml'])
    dir('target') { archive '*.jar' }
    stash name : 'binary', includes : 'target/*.jar'
  }

  stage('Tests') {
   parallel 'Unit tests': {
     withMaven(jdk: 'java-sdk8', maven: 'mvn-3.1.1', tempBinDir: '') {
       sh 'mvn clean test'
     }
     junit allowEmptyResults: true, testResults: 'target/surefire-reports/*.xml'
   }, 'Integration tests': {
     withMaven(jdk: 'java-sdk8', maven: 'mvn-3.1.1', tempBinDir: '') {
       sh 'mvn clean test-compile failsafe:integration-test'
     }
     junit allowEmptyResults: true, testResults: 'target/failsafe-reports/*.xml'
   }
 }
}
node('docker'){
  def awsId = "817926166193"
  def repoUri = "817926166193.dkr.ecr.eu-west-1.amazonaws.com/maven-repository"
  def contId = 'jenkins-app'

  stage('Build Docker img') {
    dir('build_docker') {
      unstash 'binary'
      unstash 'docker'
      sh "docker build -t jenkins/maven-app:${BUILD_NUMBER} -f Dockerfile ."
    }
  }

  stage('Validate Docker img') {    
    sh "docker stop \$(docker ps | grep ${contId} | awk '{print \$1}') || exit 0"
    sh "docker rm \$(docker ps -a | grep ${contId} | awk '{print \$1}') || exit 0"
    sh "docker run -d -p 8082:8080 --name ${contId} jenkins/maven-app:${BUILD_NUMBER} || exit 0"
  }

  stage('Prepare img for AWS') {    
    sh "pip install awscli --upgrade --user"
    sh "docker tag jenkins/maven-app:${BUILD_NUMBER} ${repoUri}:v_${BUILD_NUMBER}"
  }

  stage('Deploy AWS instance') {    
    unstash 'aws'
    sh "aws ecr get-login --region eu-west-1 --no-include-email --registry-ids ${awsId} | sh"
    sh "docker push ${repoUri}:v_${BUILD_NUMBER}"

    dir('aws'){
      sh "chmod +x jenkins_deploy_aws.sh"
      sh "./jenkins_deploy_aws.sh maven-repository maven-auto"
    }
  }
}
